var DNS = require("dns");
var telnet = require('telnet-client');

// var correo = "r.ivan.roman@hotmail.com";
// var correo = "ivan.roman@dostics.com.mx";
// var correo = "darkrirs@gmail.com";
var correo = "rodolfo.campoy@intt2.com.mx";


var dominio = correo.substring(correo.indexOf("@") + 1);

var recordsIndex = 0;

DNS.resolveMx(dominio, function(mxError, mxResponse) {
    if (mxError) {
        console.log("Hubo un error al obtener los records MX");
    } else {
        mxResponse.sort(function(a, b) {
            return a.priority - b.priority;
        });

        function getNextHost() {
            if (recordsIndex < mxResponse.length) {
                return mxResponse[recordsIndex].exchange;
            } else {
                console.log('No existen más entradas de correo.');
                process.exit(1);
            }
        }

        var mailHost = getNextHost();
        // mailHost = getNextHost();

        var connection = new telnet();


        var params = {
            host: mailHost,
            port: 25,
            shellPrompt: /.*/,
            timeout: 30000
        };

        connection.on('ready', function(prompt) {
            // console.log('prompt ', prompt);
            var saludar = function(intentos = 5) {
                // if (!intentos) {
                //     intentos = 5;
                // }

                if (intentos == 0) {
                    console.log("Intentos maximos para saludar alcanzados.");
                    process.exit(1);
                } else {
                    connection.send("helo " + dominio, function(err, response) {
                        // console.log('response ', response);
                        if (err) {
                            saludar(--intentos);
                        } else {
                            var registrarFrom = function(fromIntentos = 5) {
                                // if (!fromIntentos) {
                                //     fromIntentos = 5;
                                // }

                                if (fromIntentos == 0) {
                                    console.log("Intentos maximos para registrar from alcanzados.");
                                    process.exit(1);
                                } else {
                                    connection.send("mail from:<hola@mundo.com>", function(fromError, fromResponse) {
                                        // console.log('fromResponse ', fromResponse);
                                        if (fromError) {
                                            registrarFrom(--fromIntentos);
                                        } else {
                                            var preguntarMail = function(rcptIntentos = 5) {
                                                // if (!rcptIntentos) {
                                                //     rcptIntentos = 5;
                                                // }

                                                if (rcptIntentos == 0) {
                                                    console.log("Intentos maximos para preguntar correo alcanzados.");
                                                    process.exit();
                                                } else {
                                                    connection.send("rcpt to:<" + correo + ">", {
                                                        'timeout': 15000
                                                    }, function(rcptError, rcptResponse) {
                                                        // console.log('rcptResponse ', rcptResponse);
                                                        if (rcptError) {
                                                            preguntarMail(--rcptIntentos);
                                                        } else {
                                                            if (rcptResponse.startsWith("250")) {
                                                                console.log(correo + " TRUE");
                                                            } else {
                                                                console.log(correo + " FALSE");
                                                            }
                                                            connection.end();
                                                        }
                                                    });
                                                }
                                            };

                                            preguntarMail();
                                        }
                                    });
                                }
                            }

                            registrarFrom();
                        }
                    });
                }
            };

            saludar();
        });

        connection.on('timeout', function() {
            console.log('socket timeout!')
            connection.end();
        });

        connection.on('close', function() {
            console.log('connection closed');
        });

        connection.connect(params);
    }
});
